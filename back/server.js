var express = require('express');
var userFile = require('./users.json');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
var totalUsers = 0;
const URL_BASE = '/apitechu/v1/';
const PORT = process.env.PORT || 3000;

// Peticion GET de todos los 'users' (Collections)
app.get(URL_BASE + 'users',function(request,response){
    console.log('GET' + URL_BASE + 'users');
    response.send(userFile);
});
// Peticion GET de un 'users' (Instance)
app.get(URL_BASE + 'users/:id',function(request,response){
    console.log('GET' + URL_BASE + 'users/id');
    let indice = request.params.id;
    let instancia = userFile[indice -1];
    console.log(instancia);
    let respuesta = (instancia != undefined) ? instancia : {"mensaje":"Recurso no encontrado"};
    response.status(200); //còdgio de error HTTP
    response.send(respuesta);
});

// Peticion POST
app.post(URL_BASE + 'users',function(req,res){
    totalUsers =  userFile.length();
    cuerpo = req.body;
    if(cuerpo.length>0){
      let newUser = {
          userId : totalUsers,
          first_name : req.body.first_name,
          last_name : req.body.last_name,
          email : req.body.email,
          password : req.body.password
      }
      userFile.push(newUser);
      res.status(201); //Mensaje de alerta cuando es exitoso
      res.send({"mensaje" : "Usuario creado con éxito","usuario": newUser});

    } else {
      res.send({"mensaje" : "No se ha enviado datos Json"});
    }
});

//put nuevo actualizar
app.put(URL_BASE + 'users/:ident',function(req,res){
 let ident = req.params.ident;
 let instancia= userFile[ident - 1]; //new
 console.log(instancia);
 let datos = Object.keys(req.body).length;
 if (datos > 0){
 if (instancia != undefined)
 {
   userFile[ident-1].first_name = req.body.first_name;
   userFile[ident-1].last_name  = req.body.last_name;
   userFile[ident-1].email      = req.body.email;
   userFile[ident-1].password   = req.body.password;
   console.log(userFile[ident-1]);

 res.status(200);
 res.send({"mensaje":"Usuario actualizado con exito ", "usuario" : userFile[ident-1]});
 }
 else {
   res.status(200);
   res.send({"mensaje":"Usuario no existe "});
 }
}//new
else {
 res.status(200);
 res.send({"mensaje":"Ingrese datos a actualizar "});
}  //new
}
);
//put nuevo fin

//LUIS ESTRADA - COPIA DE VERSION
app.delete(URL_BASE + 'users',function(req,res){
      if(reqEmpty(req)){
        console.log('Usuario a eliminar' + JSON.stringify(req.body.userId));
        userFile.splice(req.body.id - 1,1);
        res.status(200);
        res.send({"mensaje": "usuario eliminado " + JSON.stringify(userFile)});

      } else {
        res.send({"mensaje": "No se tiene body"});
      }
});

//FUNCTION PARA IDENTIFICAR SI LLEGA DATOS EN JSON
function reqEmpty(req){
  return Object.keys(req.body).length !== 0 ? true : false;
}

//LOGIN - WRITEUSERDATATOFILE

app.post(URL_BASE + 'login',function(req,res){
  var email = req.body.email;
  var pass  = req.body.password;
  for (us of userFile){
    if (us.email == email) {
      if (us.password == pass) {
        us.logged = true;
        writeUserDataToFile(userFile);
        console.log("Usuario Logeado: " + us.email);
        res.send({"mensaje": "usuario logeado.", "idUsuario" : us.id, "logeado" : "true"});
      }else {
        console.log("Login incorrecto");
        res.send({"msg": "Login Incorrecto. ","pass ": us.pass});
      }
    }
  }
});

//LOGOUT SIN PARAMETRO BUSQUEDA POR ID
app.post(URL_BASE + 'logout',function(req,res){
  var id = req.body.id;
  for (us of userFile){
    if (us.id == id) {
      if (us.logged) {
        delete us.logged;
        writeUserDataToFile(userFile);
        console.log("Usuario Logout: " + us.email);
        res.send({"mensaje": "usuario Logout.", "idUsuario" : us.id, "Logout" : "true"});
      }else {
        console.log("Usuario se encuentra logeado");
        res.send({"msg": "Usuario no se encuentra logeado. ","IdUser ": us.id});
      }
    }
  }
});

function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./users.json",jsonUserData,"utf8",function(err){
    if(err){
      console.log(err);
    } else{
      console.log("Datos escritos en 'users.json'.");
    }
  })
};

//GENERACION DE ARCHIVO LOGEADOS CON GET
app.get(URL_BASE + 'loggeds',function(req,res){
    var logados=new Array();
    for(user of userFile){
      if(user.logged){
        logados.push(user);
      }
    }
    res.send(logados);
});

//GENERACION DE ARCHIVO LOGEADOS --> LOGGED=TRUE
app.post(URL_BASE + 'logeados',function(req,res){
 var escritos = 0;
 let arreglo = [];
 for (us of userFile){
   if (us.logged){
     arreglo.push(us);
     escritos = escritos + 1;
   }
 }
 escribirUsuariosLogeados(arreglo);
 console.log(escritos);
 res.send({"Total escritos" : escritos});
});

function escribirUsuariosLogeados(data){
 var fs = require('fs');
 var jsonUserData = JSON.stringify(data);
 fs.writeFile("./logeados.json",jsonUserData,"utf8",
function(err){
   if (err){
     console.log(err);
   } else {
     console.log("datos escritos en archivo");
   }
 })
};

// Peticion GET con query String
app.get(URL_BASE + 'usersq',function(request,response){
    console.log('GET' + URL_BASE + 'con query string');
    console.log(request.query.valor1);
    console.log(request.query.valor2);
    response.send(respuesta);
});

// Peticion GET con query String muestra rango de clientes
app.get(URL_BASE + 'ranksq',function(req,res){
    console.log('GET' + URL_BASE + 'con query string');
    console.log(req.query.inf);
    console.log(req.query.sup);
    var inf = parseInt(req.query.inf,10);
    var sup = parseInt(req.query.sup,10);
    var max = userFile.length;
    var inf_valido = false;
    var sup_valido = false;
    var rank= new Array();

    if (inf > max){
      console.log("El limite inferior del rango no puede ser mayor al número de elementos");
      res.send({"msg":"El limite inferior: " + inf + " no puede ser mayor al número de elementos: " + max});
    }else{
      if(inf < 1){
        console.log("El limite inferior del rango no puede ser menor a 1");
        res.send({"msg":"El limite inferior: " + inf + " no puede ser menor a 1"});
      }else{
        if(inf > sup){
          console.log("el lìmite inferior:" + inf + " no puede ser mayor al limite superior: "+sup);
          res.send({"msg":"El limite inferior: " + inf + " no puede ser mayor al limite superior: " + sup});
        }else{
          inf_valido = true;
        }
      }
    }

    if (sup > max){
      console.log("El limite superior del rango no puede ser mayor al número de elementos");
      res.send({"msg":"El limite superior: " + sup + " no puede ser mayor al número de elementos: " + max});
    }else{
      if(sup < 1){
        console.log("El limite superior del rango no puede ser menor a 1");
        res.send({"msg":"El limite superior: " + sup + " no puede ser menor a 1"});
      }else{
         if(inf > sup){
          console.log("el lìmite inferior:" + inf + " no puede ser mayor al limite superior: "+sup);
          res.send({"msg":"El limite inferior: " + inf + " no puede ser mayor al limite superior: " + sup});
        }else{
          sup_valido = true;
        }
      }
    }
    if (inf_valido && sup_valido){
      for (i=inf;i<=sup;i++){
        rank.push(userFile[i-1]);
      }
      console.log("correcto");
      res.send({"msg":"valores correctos" + JSON.stringify(rank)});
    }
});

app.listen(3000, function() {
    console.log('Api escuchando en puerto 3000...');
});
